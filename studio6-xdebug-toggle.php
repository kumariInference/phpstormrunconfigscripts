<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

use phpseclib\Net\SSH2;

$ssh = new SSH2('studio-kumar-01.lab.mel.inferencecommunications.com');
if (!$ssh->login('kumar', NORMAL_PASS)) {
    exit('Login Failed');
}
echo $ssh->exec("php xdebug-toggle.php");
?>