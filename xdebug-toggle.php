<?php
define('PATH_TO_PHP_INI', 'C:\Users\bhara\Documents\Work\Xampp\php\php.ini');
define('REXP_EXTENSION', '(zend_extension\s*=.*?php_xdebug)');

$s = file_get_contents(PATH_TO_PHP_INI);
$replaced = preg_replace('/;' . REXP_EXTENSION . '/', '$1', $s);
$isOn = $replaced != $s;
if (!$isOn) {
    $replaced = preg_replace('/' . REXP_EXTENSION . '/', ';$1', $s);
}
file_put_contents(PATH_TO_PHP_INI, $replaced);
echo 'xdebug is ' . ($isOn ? 'ON' : 'OFF') . " \n";
?>