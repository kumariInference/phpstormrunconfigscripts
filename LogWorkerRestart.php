<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

use phpseclib\Net\SSH2;

$ssh = new SSH2('studio-app-05.lab.mel.inferencecommunications.com');
if (!$ssh->login('kumar', ONE_PASS)) {
    exit('Login Failed');
}
echo $ssh->exec("echo ". ONE_PASS. " | sudo -S supervisorctl restart LogWorker06:");
?>