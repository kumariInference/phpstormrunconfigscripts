<?php

define('PATH_TO_PHP_INI', '/etc/php.d/xdebug.ini');
define('REXP_EXTENSION', '(zend_extension\s*=.*?xdebug)');

$s = file_get_contents(PATH_TO_PHP_INI);
$replaced = preg_replace('/;' . REXP_EXTENSION . '/', '$1', $s);
$isOn = $replaced != $s;
if (!$isOn) {
    $replaced = preg_replace('/' . REXP_EXTENSION . '/', ';$1', $s);
}
file_put_contents(PATH_TO_PHP_INI, $replaced);
exec("echo " . NORMAL_PASS . " | sudo -S systemctl restart php-fpm");
echo 'xdebug is ' . ($isOn ? 'ON' : 'OFF') . " \n";
